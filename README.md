![](https://yt3.ggpht.com/ytc/AKedOLTjSvgBgtLmvQSNuuP-z22LFql2QOlcweAzH50-GW8=s88-c-k-c0x00ffffff-no-rj)

# About Me

Technology and gadgets are my passion! After spending my career in IT, I now spend my days reading tech news and blogging daily about what I find interesting. I try to transform the techspeak into something that ordinary non-tech folk can see a use for. My interests lean toward open source, green technology, security, privacy, amateur radio (ZS1OSS), alternative social networks, etc.

----
# Follow Me

I blog daily (about 3 to 5 posts pd) on various social networks such as:
- Mastodon: <a rel="me" href="https://mastodon.social/@danie10">Danie10</a>
- Twitter: https://twitter.com/danie10
- Friendica: https://squeet.me/profile/danie10/
- Hubzilla: https://hub.vilarejo.pro.br/channel/gadgeteer
- Reddit: https://www.reddit.com/r/GadgeteerZA/
- Linkedin: https://www.linkedin.com/company/gadgeteerza/
- Minds: https://www.minds.com/danievdm/
- WordPress (option to subscribe to e-mails): https://gadgeteer.co.za/blog/
- RSS feed: https://gadgeteer.co.za/category/uncategorized/feed/
- Pixelfed: https://pixelfed.social/i/web/profile/9484
- Secure Scuttlebutt (P2P): @0Tv5f/opjv/m1ONAKl6S79HWbHdLaCtt/emdXl2MCO0=.ed25519
- Matrix Room (also bridged to my IRC channel): https://matrix.to/#/#gadgeteerza:libera.chat
- XMPP Chatroom: gadgeteerza-tech-blog@conference.movim.eu
- XMPP Community Channel: https://mov.im/?community/news.movim.eu/gadgeteerza-tech-blog

----
# 📰 Recent Blog Posts
<!-- BLOG-POST-LIST:START -->
- [Ukama: Be your own cell carrier, using your internet connection even when you’re away from home](https://gadgeteer.co.za/ukama-be-your-own-cell-carrier-using-your-internet-connection-even-when-youre-away-from-home/)
- [AI eye-scanner can tell whether you’ll croak it from a heart attack – maybe soon blood tests may not be required and testing much quicker](https://gadgeteer.co.za/ai-eye-scanner-can-tell-whether-youll-croak-it-from-a-heart-attack-maybe-soon-blood-tests-may-not-be-required-and-testing-much-quicker/)
- [Debian Linux accepts proprietary firmware in major policy change because the world is full of compromises, and you already bought the proprietary hardware](https://gadgeteer.co.za/debian-linux-accepts-proprietary-firmware-in-major-policy-change-because-the-world-is-full-of-compromises-and-you-already-bought-the-proprietary-hardware/)
- [Google adds voice dictation support for 8 South African languages and for Rwanda](https://gadgeteer.co.za/google-adds-voice-dictation-support-for-8-south-african-languages-and-for-rwanda/)
- [Google Camera Port Hub: Get the best GCam APK for Samsung, Xiaomi, Redmi, and other phones!](https://gadgeteer.co.za/google-camera-port-hub-get-the-best-gcam-apk-for-samsung-xiaomi-redmi-and-other-phones/)
<!-- BLOG-POST-LIST:END -->

----
# Videos

I also publish videos mostly about technology, open source and alternative social media at:
- YouTube: https://www.youtube.com/DanievanderMerwe
- Odysee: https://odysee.com/@GadgeteerZA:4
- Peertube: https://video.hardlimit.com/c/gadgeteerza/videos?languageOneOf=af&languageOneOf=nl&languageOneOf=en&s=2

----
# Coding

Although I've not coded for many years, I recently restarted again in 2021 with Python. Python has long been on my bucket list as a general programming language to learn, and I must say if you've learnt any other language previously, Python is not difficult to get going with. So as, and when, I need some utility to use, and it's not available elsewhere, I'll be doing that in Python. I started out with a GUI manager on LInux for Cloudflare WARP, and will see where I end up with Python. 
